package com.example.jalbe.exam3p;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class terceraronda extends AppCompatActivity implements View.OnClickListener {

    ImageButton imbCarta1, imbCarta2, imbCarta3;
    ArrayList<Integer> imagenes =  new ArrayList<>(Arrays.asList(R.mipmap.diez,R.mipmap.cero,R.mipmap.cero));
    int[] juego = new int[3];
    int[] cartas_selecionadas = new int[2];
    int[] imagenes_selecionadas = new int[2];
    int[] juego_terminado = new int[3];
    int turno=0, ganador=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_terceraronda);

        imbCarta1 = (ImageButton) findViewById(R.id.imbCarta1);
        imbCarta1.setOnClickListener(this);
        imbCarta2 = (ImageButton) findViewById(R.id.imbCarta2);
        imbCarta2.setOnClickListener(this);
        imbCarta3 = (ImageButton) findViewById(R.id.imbCarta3);
        imbCarta3.setOnClickListener(this);



        if (savedInstanceState == null)
        {
            asignarImagenes();
            for (int i=0;i<3;i++)
                juego_terminado[i]=0;

            cartas_selecionadas[0]=3;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_memorama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.acercade) {
            Intent acercade = new Intent(this, Acercade.class);
            startActivity(acercade);
            return true;
        }

        if (id == R.id.juegonuevo) {
            Intent memorama = new Intent(this, MainActivity.class);
            startActivity(memorama);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void asignarImagenes()
    {
        int posicion, contador = 0;



        for (int i = 0; i < 3; i++) {

            posicion = (int) (Math.random() * imagenes.size());

            juego[i] = imagenes.get(posicion);

            imagenes.remove(posicion);

        }


    }

    @Override
    public void onClick(View v){

        switch (v.getId())
        {
            case R.id.imbCarta1:
                imbCarta1.setImageResource(juego[0]);
                imagenes_selecionadas[turno]=juego[0];
                cartas_selecionadas[turno]=0;
                if(juego[0]== R.mipmap.tickets1 ){
                    Toast.makeText(getApplicationContext(),"Ticket Especial",Toast.LENGTH_SHORT).show();
                    Intent tercera = new Intent(this, terceraronda.class);
                    startActivity(tercera);

                }
                imbCarta1.setEnabled(false);
                break;
            case R.id.imbCarta2:
                imbCarta2.setImageResource(juego[1]);
                imagenes_selecionadas[turno]=juego[1];
                cartas_selecionadas[turno]=1;
                if(juego[1]== R.mipmap.tickets1 ){
                    Toast.makeText(getApplicationContext(),"Ticket Especial",Toast.LENGTH_SHORT).show();
                    Intent tercera = new Intent(this, terceraronda.class);
                    startActivity(tercera);

                }
                imbCarta2.setEnabled(false);
                break;
            case R.id.imbCarta3:
                imbCarta3.setImageResource(juego[2]);
                imagenes_selecionadas[turno]=juego[2];
                cartas_selecionadas[turno]=2;
                if(juego[2]== R.mipmap.tickets1){
                    Toast.makeText(getApplicationContext(),"Ticket Especial",Toast.LENGTH_SHORT).show();
                    Intent tercera = new Intent(this, terceraronda.class);
                    startActivity(tercera);

                }
                imbCarta3.setEnabled(false);
                break;


        }

        if(turno==0) {
            turno = 1;
        }else{
            new terceraronda.Hilo().execute();
            turno=0;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle estado) {
        super.onSaveInstanceState(estado);
        estado.putInt("TURNO", turno);
        estado.putInt("GANADOR", ganador);
        estado.putIntArray("ARREGLO_JUEGO", juego);
        estado.putIntArray("ARREGLO_JUEGO_TERMINADO", juego_terminado);
        estado.putIntArray("ARREGLO_CARTAS_SELECCIONADAS", cartas_selecionadas);
        estado.putIntArray("ARREGLO_IMAGENES_SELECCIONADAS", imagenes_selecionadas);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        turno = savedInstanceState.getInt("TURNO");
        ganador = savedInstanceState.getInt("GANADOR");
        juego = savedInstanceState.getIntArray("ARREGLO_JUEGO");
        juego_terminado = savedInstanceState.getIntArray("ARREGLO_JUEGO_TERMINADO");
        cartas_selecionadas = savedInstanceState.getIntArray("ARREGLO_CARTAS_SELECCIONADAS");
        imagenes_selecionadas = savedInstanceState.getIntArray("ARREGLO_IMAGENES_SELECCIONADAS");

        if (juego_terminado[0] == 1 || cartas_selecionadas[0]==0)
        {
            imbCarta1.setImageResource(juego[0]);
            imbCarta1.setEnabled(false);
        }

        if (juego_terminado[1] == 1 || cartas_selecionadas[0]==1)
        {
            imbCarta2.setImageResource(juego[1]);
            imbCarta2.setEnabled(false);
        }

        if (juego_terminado[2] == 1 || cartas_selecionadas[0]==2)
        {
            imbCarta3.setImageResource(juego[2]);
            imbCarta3.setEnabled(false);
        }



    }

    class Hilo extends AsyncTask<Void,Integer,Void>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            imbCarta1.setEnabled(false);
            imbCarta2.setEnabled(false);
            imbCarta3.setEnabled(false);


        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(300);

            }catch (InterruptedException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(imagenes_selecionadas[0]==imagenes_selecionadas[1])
            {
                juego_terminado[cartas_selecionadas[0]]=1;
                juego_terminado[cartas_selecionadas[1]]=1;

                ganador++;

                if(ganador==4){
                    Toast.makeText(getApplicationContext(),"!Felicidades has ganado¡",Toast.LENGTH_SHORT).show();
                }

            }

            if (juego_terminado[0] == 0)
            {
                imbCarta1.setImageResource(R.mipmap.logo);
                imbCarta1.setEnabled(true);
            }

            if (juego_terminado[1] == 0)
            {
                imbCarta2.setImageResource(R.mipmap.logo);
                imbCarta2.setEnabled(true);
            }

            if (juego_terminado[2] == 0)
            {
                imbCarta3.setImageResource(R.mipmap.logo);
                imbCarta3.setEnabled(true);
            }





            cartas_selecionadas[0]=7;

        }
    }

}
